package ru.t1.artamonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataYamlSaveFasterXmlRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class DataYamlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-yaml";

    @NotNull
    private static final String DESCRIPTION = "Save data in yaml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE YAML]");
        @NotNull DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(getToken());
        domainEndpointClient.saveDataYamlFasterXml(request);
    }

}
