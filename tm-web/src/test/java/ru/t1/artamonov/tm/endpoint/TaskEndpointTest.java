package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.client.ITaskEndpointClient;
import ru.t1.artamonov.tm.marker.IntegrationCategory;
import ru.t1.artamonov.tm.model.Task;

import java.util.Collection;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    final static String BASE_URL = "http://localhost:8080/api/tasks";

    @NotNull
    final ITaskEndpointClient taskClient = ITaskEndpointClient.taskClient(BASE_URL);

    @NotNull
    final Task task1 = new Task("Task1");

    @NotNull
    final Task task2 = new Task("Task2");

    @NotNull
    final Task task3 = new Task("Task3");

    @Before
    public void init() {
        taskClient.save(task1);
        taskClient.save(task2);
    }

    @After
    public void clean() {
        taskClient.delete(task1);
        taskClient.delete(task2);
        taskClient.delete(task3);
    }

    @Test
    public void findAll() {
        Collection<Task> tasks = taskClient.findAll();
        Assert.assertTrue(tasks.size() >= 2);
    }

    @Test
    public void findById() {
        @NotNull final Task task = taskClient.findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void delete() {
        Assert.assertNotNull(taskClient.findById(task1.getId()));
        taskClient.delete(task1);
        Assert.assertNull(taskClient.findById(task1.getId()));
    }

    @Test
    public void deleteById() {
        Assert.assertNotNull(taskClient.findById(task2.getId()));
        taskClient.delete(task2);
        Assert.assertNull(taskClient.findById(task2.getId()));
    }

    @Test
    public void save() {
        taskClient.save(task3);
        Assert.assertEquals(task3.getId(), taskClient.findById(task3.getId()).getId());
    }

}
