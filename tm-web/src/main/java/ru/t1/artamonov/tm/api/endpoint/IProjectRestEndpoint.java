package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Project;

import java.util.Collection;

public interface IProjectRestEndpoint {

    void delete(Project project);

    void delete(String id);

    @Nullable
    Collection<Project> findAll();

    @Nullable
    Project findById(String id);

    @NotNull
    Project save(Project project);

}
