package ru.t1.artamonov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.model.Session;


@Repository
public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session findByUserIdAndId(String userId, String id);

    @Transactional
    void deleteByUserId(String userId);

}
