package ru.t1.artamonov.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.artamonov.tm.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.artamonov.tm")
public class LoggerConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory(@NotNull final IPropertyService propertyService) {
        return new ActiveMQConnectionFactory(propertyService.getBrokerUrl());
    }

}
